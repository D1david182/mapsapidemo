//
//  ColorPalette.swift
//  MapApplication
//
//  Created by Daniel Davidson on 3/11/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//
import UIKit

let primaryOrange = UIColor(red:1.00, green:0.30, blue:0.24, alpha:1.0)
let primaryBlue = UIColor(red:0.07, green:0.18, blue:0.36, alpha:1.0)



extension UIButton {
    
    func pulsate() {
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.15
        pulse.fromValue = 0.8
        pulse.toValue = 1.0
        pulse.autoreverses = false
        pulse.repeatCount = 0
        pulse.initialVelocity = 1
        pulse.damping = 1.0
        
        layer.add(pulse, forKey: "pulse")
    }
}
