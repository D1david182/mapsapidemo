//
//  MapDataSource.swift
//  MapApplication
//
//  Created by Daniel Davidson on 3/11/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.

import UIKit
import GooglePlaces
import GoogleMaps
import CoreLocation
import SwiftyJSON

class MapDataSource : NSObject {
    
    var placesClient: GMSPlacesClient!
    let googleApiKey = "AIzaSyDzSbX8xCkMX6egmT-2dFCTYm4DpYOP734" //Normally would store in safe place.
    var downloadTask : URLSessionDataTask?
    var session : URLSession{return URLSession.shared}
    
    override init() {
        super.init()
    }
    
    func retrieveRestaurants(coordinate: CLLocationCoordinate2D, radius: Double, type: String, completion: @escaping ([GoogleResult]) -> Void) {
        
        var urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(coordinate.latitude),\(coordinate.longitude)&radius=1500&type=restaurant&keyword=\(type)&key=\(googleApiKey)"
        
        
        //If invalid return empty array
        guard let url = URL(string: urlString) else {
            completion([])
            return
        }
        //Cancel existing tasks
        if let task = downloadTask, task.taskIdentifier > 0 && task.state == .running {
            task.cancel()
        }
        
        //Download json data and parse
        downloadTask = session.dataTask(with: url) { data, response, error in
            var placesArray: [GoogleResult] = []
            defer {
                DispatchQueue.main.async {
                    completion(placesArray)
                }
            }
            guard let data = data,
                let json = try? JSON(data: data, options: .mutableContainers),
                let results = json["results"].arrayObject as? [[String: Any]] else {
                    return
            }
            //Get image associated with each item
            results.forEach {
                var place = GoogleResult(dictionary: $0, acceptedTypes: type)
                if let reference = place.photoReference {
                    self.fetchPhotoFromReference(reference) { imageURL in
                        place.photo = imageURL?.absoluteString
                        placesArray.append(place)
                    }
                }else{
                    placesArray.append(place)
                }
            }
        }
        downloadTask?.resume()
    }
    func retrieveCity(cityName : String, completion: @escaping ([GoogleResult]) -> Void) {
        
        var urlString = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=\(cityName)&sensor=false&key=\(googleApiKey)"
        
        //If invalid return empty array
        guard let url = URL(string: urlString) else {
            completion([])
            return
        }
        //Cancel existing tasks
        if let task = downloadTask, task.taskIdentifier > 0 && task.state == .running {
            task.cancel()
        }
        
        //Download json data and parse
        downloadTask = session.dataTask(with: url) { data, response, error in
            var placesArray: [GoogleResult] = []
            defer {
                DispatchQueue.main.async {
                    completion(placesArray)
                }
            }
            guard let data = data,
                let json = try? JSON(data: data, options: .mutableContainers),
                let results = json["results"].arrayObject as? [[String: Any]] else {
                    return
            }
            //Get image associated with each item
            results.forEach {
                var place = GoogleResult(dictionary: $0, acceptedTypes: "locality")
                if let reference = place.photoReference {
                    self.fetchPhotoFromReference(reference) { imageURL in
                        place.photo = imageURL?.absoluteString
                        placesArray.append(place)
                    }
                }else{
                    placesArray.append(place)
                }
            }
        }
        downloadTask?.resume()
    }
    func fetchPhotoFromReference(_ reference: String, completion: @escaping (URL?) -> Void) {
        
        let urlString = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=\(reference)&key=\(googleApiKey)"
        guard let url = URL(string: urlString) else {
            completion(nil)
            return
        }
        completion(url)
    }
}
struct GoogleResult {
    let name: String
    let address: String
    let coordinate: CLLocationCoordinate2D
    let placeType: String
    var photoReference: String?
    var photo: String?
    var rating : String?
    
    init(dictionary: [String: Any], acceptedTypes: String)
    {
        let json = JSON(dictionary)
        name = json["name"].stringValue
        address = json["vicinity"].stringValue
        rating = json["rating"].stringValue
        let lat = json["geometry"]["location"]["lat"].doubleValue as CLLocationDegrees
        let lng = json["geometry"]["location"]["lng"].doubleValue as CLLocationDegrees
        coordinate = CLLocationCoordinate2DMake(lat, lng)
        
        photoReference = json["photos"][0]["photo_reference"].string
        placeType = "restaurant"
    }
}
