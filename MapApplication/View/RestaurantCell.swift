//
//  RestaurantCell.swift
//  MapApplication
//
//  Created by Daniel Davidson on 3/12/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import UIKit
import SDWebImage

class RestaurantCell : UICollectionViewCell {
    
    var ResultDetails : GoogleResult?{
        didSet{
            if let url = ResultDetails?.photo {
              mainImage.sd_setImage(with: URL.init(string: url), placeholderImage: nil, options: .scaleDownLargeImages, completed: nil)
            }else{
                mainImage.image = #imageLiteral(resourceName: "burger")
            }
            titleLabel.text = ResultDetails?.name
            setSubTitle()
        }
    }
    lazy var mainImage : UIImageView = {
       let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = 9
        image.clipsToBounds = true
        return image
    }()
    lazy var titleLabel : UILabel = {
       let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = primaryOrange
        lbl.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        return lbl
    }()
    lazy var subLabel : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = primaryBlue
        lbl.numberOfLines = 0
        return lbl
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layer.cornerRadius = 15
        layer.shadowRadius = 3
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        setupComponents()
    }
    fileprivate func setupComponents(){
        addSubview(mainImage)
        addSubview(titleLabel)
        addSubview(subLabel)
        
        mainImage.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        mainImage.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        mainImage.heightAnchor.constraint(equalToConstant: 80).isActive = true
        mainImage.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
        titleLabel.leftAnchor.constraint(equalTo: mainImage.rightAnchor, constant: 8).isActive = true
        titleLabel.topAnchor.constraint(equalTo: mainImage.topAnchor).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -5).isActive = true
        
        subLabel.leftAnchor.constraint(equalTo: mainImage.rightAnchor, constant: 8).isActive = true
        subLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: -5).isActive = true
        subLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -5).isActive = true
        subLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2).isActive = true
        
    }
    fileprivate func setSubTitle() {
        //Set address and append rating if available
        var addressString: NSMutableAttributedString?
        if let address = ResultDetails?.address {
            addressString = NSMutableAttributedString(string: address, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .light)])
        }
        if let rating = ResultDetails?.rating{
            let ratingString = NSMutableAttributedString(string: "\nRating: \(rating)", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)])
            addressString?.append(ratingString)
        }
        subLabel.attributedText = addressString
    }
    override func prepareForReuse() {
        mainImage.image = nil
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
