//
//  WelcomeView.swift
//  MapApplication
//
//  Created by Daniel Davidson on 3/12/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import UIKit

protocol WelcomeMessageDelegate : class{
    func handleHideWelcomeMessage()
}

class WelcomeView : UIView {
    
    weak var delegate : WelcomeMessageDelegate?
    
    lazy var titleLabel : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        lbl.textAlignment = .center
        let attributedText = NSMutableAttributedString(string: "Welcome!\n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20, weight: .bold)])
        let subText = NSMutableAttributedString(string: "Lets get some lunch\n", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .light)])
        attributedText.append(subText)
        lbl.attributedText = attributedText
        lbl.numberOfLines = 0
        return lbl
    }()
    let startButton : UIButton = {
       let btn = UIButton()
        btn.setTitle("Start", for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        btn.setTitleColor(primaryBlue, for: .normal)
        btn.backgroundColor = .white
        btn.layer.cornerRadius = 6
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.shadowRadius = 3
        btn.layer.shadowColor = UIColor.darkGray.cgColor
        btn.layer.shadowOpacity = 0.5
        btn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        btn.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        return btn
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupComponents()
        backgroundColor = primaryBlue
        layer.shadowRadius = 3
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.cornerRadius = 9
    }
    fileprivate func setupComponents(){
        addSubview(titleLabel)
        addSubview(startButton)
        
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -20).isActive = true
        
        startButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        startButton.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5).isActive = true
        startButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        startButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
    }
    @objc func handleDismiss(sender : UIButton){
        sender.pulsate()
        delegate?.handleHideWelcomeMessage()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
