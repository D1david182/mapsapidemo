//
//  MapAnnotation.swift
//  MapApplication
//
//  Created by Daniel Davidson on 3/12/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

//Custom map annotation to allow extra attributes to be associated with the pins
class customAnnotationView: NSObject, MKAnnotation {
    let title: String?
    let coordinate: CLLocationCoordinate2D
    let rating : String?
    let image : UIImage?
    let color : UIColor?
    
    init(title: String,coordinate: CLLocationCoordinate2D, image: UIImage, color : UIColor, rating: String) {
        self.title = title
        self.coordinate = coordinate
        self.rating = rating
        self.image = image
        self.color = color
        super.init()
    }
    var subtitle: String? {
        return "Rating: \(rating ?? "None")"
    }
}
