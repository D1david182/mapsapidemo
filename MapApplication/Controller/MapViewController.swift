//
//  ViewController.swift
//  MapApplication
//
//  Created by Daniel Davidson on 3/11/19.
//  Copyright © 2019 Daniel Davidson. All rights reserved.
//

//Thanks for testing my demo!!!

import UIKit
import MapKit
import CoreLocation
import GooglePlaces

class MapViewController: UIViewController,CLLocationManagerDelegate, UISearchBarDelegate, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MKMapViewDelegate, WelcomeMessageDelegate {
    
    var cellID = "cellID"
    var defaultCity = "Tempe"
    var resultsArray = [GoogleResult]()
    let GoogleDataSource = MapDataSource()
    var locationManager = CLLocationManager()
    var currentLocation = CLLocationCoordinate2D(latitude: 33.42135, longitude: -111.942544)
    
    lazy var mapView : MKMapView = {
        let map = MKMapView()
        map.translatesAutoresizingMaskIntoConstraints = false
        map.showsCompass = false
        map.delegate = self
        return map
    }()
    lazy var mainCollection : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.register(RestaurantCell.self, forCellWithReuseIdentifier: cellID)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.alwaysBounceVertical = true
        collection.showsVerticalScrollIndicator = false
        collection.backgroundColor = .clear
        collection.dataSource = self
        collection.delegate = self
        collection.isHidden = true
        return collection
    }()
    lazy var loadingWheel : UIActivityIndicatorView = {
        let wheel = UIActivityIndicatorView()
        wheel.translatesAutoresizingMaskIntoConstraints = false
        wheel.style = .whiteLarge
        wheel.color = primaryBlue
        wheel.hidesWhenStopped = true
        return wheel
    }()
    lazy var searchBar : UISearchBar = {
        let bar = UISearchBar()
        bar.translatesAutoresizingMaskIntoConstraints = false
        bar.searchBarStyle = .minimal
        bar.barTintColor = .white
        bar.placeholder = "Search here..."
        bar.delegate = self
        if let textfield = bar.value(forKey: "searchField") as? UITextField {
            textfield.textColor = UIColor.white
            textfield.backgroundColor = primaryOrange
            if let backgroundview = textfield.subviews.first {
                backgroundview.backgroundColor = primaryBlue.withAlphaComponent(0.8)
                backgroundview.layer.cornerRadius = 10
                backgroundview.clipsToBounds = true
            }
            let placeHolder = textfield.value(forKey: "placeholderLabel") as? UILabel
            placeHolder?.textColor = UIColor.white
        }
        return bar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = primaryOrange
        
        requestLocationPermissions()
        setupComponents()
        styleNavBar()
        retrieveInitialLoadFood(location: currentLocation)
        showWelcomeMessage()
    }
    
    fileprivate func setupComponents(){
        view.addSubview(mapView)
        mapView.addSubview(loadingWheel)
        mapView.addSubview(mainCollection)
        mapView.addSubview(searchBar)
        
        mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        mapView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        loadingWheel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        loadingWheel.widthAnchor.constraint(equalToConstant: 50).isActive = true
        loadingWheel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loadingWheel.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 50).isActive = true
        
        searchBar.topAnchor.constraint(equalTo: mapView.topAnchor, constant: 15).isActive = true
        searchBar.leftAnchor.constraint(equalTo: mapView.leftAnchor, constant: 20).isActive = true
        searchBar.rightAnchor.constraint(equalTo: mapView.rightAnchor, constant: -20).isActive = true
        searchBar.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        mainCollection.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 25).isActive = true
        mainCollection.leftAnchor.constraint(equalTo: mapView.leftAnchor, constant: 20).isActive = true
        mainCollection.rightAnchor.constraint(equalTo: mapView.rightAnchor, constant: -20).isActive = true
        mainCollection.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    ////////////////DataSource Functions////////////////////////
    fileprivate func retrieveInitialLoadFood(location : CLLocationCoordinate2D){
        loadingWheel.startAnimating()
        GoogleDataSource.retrieveRestaurants(coordinate: location, radius: 2500, type: "food") { (result) in
            self.resultsArray = result
            self.reloadMapRegions()
            self.loadingWheel.stopAnimating()
        }
    }
    fileprivate func retrieveRestaurantsOnSearch(location: CLLocationCoordinate2D, type: String){
        self.resultsArray.removeAll()
        self.mainCollection.reloadData()
        loadingWheel.startAnimating()
        GoogleDataSource.retrieveRestaurants(coordinate: location, radius: 2500, type: type.lowercased()) { (result) in
            self.view.endEditing(true)
            self.mainCollection.isHidden = false
            self.loadingWheel.stopAnimating()
            DispatchQueue.main.async {
                self.mainCollection.performBatchUpdates({
                    self.resultsArray.insert(contentsOf: result, at: 0)
                    for i in 0..<self.resultsArray.count{
                        self.mainCollection.insertItems(at: [IndexPath(item: i, section: 0)])
                    }
                })
                self.reloadMapRegions()
            }
        }
    }
    ////////////////Location Delegate Functions////////////////////////
    fileprivate func requestLocationPermissions(){
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        let viewRegion = MKCoordinateRegion.init(center: currentLocation, latitudinalMeters: 1000, longitudinalMeters: 1000)
        mapView.setRegion(viewRegion, animated: true)
        mapView.camera.pitch = 100
        mapView.camera.altitude = 800
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        currentLocation = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        CLGeocoder().reverseGeocodeLocation(location!) { (placemark, error) in
            if error != nil {
                print("Error occured reversing location")
            }else{
                if let place = placemark?.last{
                    if self.defaultCity.lowercased() != place.locality?.lowercased(){
                        self.retrieveInitialLoadFood(location: self.currentLocation)
                    }
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        manager.requestLocation()
    }
    
    ////////////////Searchbar Functions////////////////////////
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let keyword = searchBar.text else {return}
        if !keyword.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty{
            retrieveRestaurantsOnSearch(location: currentLocation, type: keyword)
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //Only reset if array has items else just hide
        if searchBar.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty{
            if mainCollection.visibleCells.count > 0 {
                resetArrayAndHideCollection()
            }else{
                hideCollection()
            }
        }
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if mainCollection.visibleCells.count > 0 {
            showCollection()
        }
    }
    
    ////////////////CollectionView Functions////////////////////////
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! RestaurantCell
        cell.ResultDetails = resultsArray[indexPath.item]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return resultsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 50, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        view.endEditing(true)
        let location = resultsArray[indexPath.item].coordinate
        let viewRegion = MKCoordinateRegion.init(center: location, latitudinalMeters: 500, longitudinalMeters: 500)
        hideCollection()
        mapView.setRegion(viewRegion, animated: true)
        
        let selectedResult = resultsArray[indexPath.item]
        for mapPin in mapView.annotations{
            let customPin = mapPin as? customAnnotationView
            if selectedResult.name == customPin?.title{
                mapView.selectAnnotation(mapPin, animated: true)
            }
        }
    }
    fileprivate func resetArrayAndHideCollection() {
        
        self.mainCollection.performBatchUpdates({
            for index in stride(from: resultsArray.count - 1, through: 0, by: -1){
                self.resultsArray.remove(at: index)
                self.mainCollection.deleteItems(at: [IndexPath(item: index, section: 0)])
            }
        }) { (completion) in
            self.mainCollection.isHidden = true
        }
    }
    fileprivate func hideCollection(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.mainCollection.alpha = 0
        }) { (completion) in
            self.mainCollection.isHidden = true
        }
    }
    fileprivate func showCollection(){
        mainCollection.isHidden = false
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.mainCollection.alpha = 1
        }) { (completion) in
            
        }
    }
    ////////////////MapView Functions////////////////////////
    func reloadMapRegions(){
        mapView.removeAnnotations(mapView.annotations)
        
        for item in resultsArray {
            let latitude = Double(item.coordinate.latitude)
            let longitude = Double(item.coordinate.longitude)
            let title = item.name
            let rating = item.rating
            
            let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let annotation = customAnnotationView(title: title , coordinate: location, image: #imageLiteral(resourceName: "restaurant-interface-symbol-of-fork-and-knife-couple"), color: primaryBlue, rating: rating ?? "None")
            
            self.mapView.addAnnotation(annotation)
        }
    }
    
    let clusterIdentifier = "Cluster"
    let identifier = "marker"
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? customAnnotationView {
            
            var view: MKMarkerAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                as? MKMarkerAnnotationView {
                
                dequeuedView.annotation = annotation
                view = dequeuedView
                view.glyphImage = annotation.image
                view.selectedGlyphImage = annotation.image
                view.clusteringIdentifier = clusterIdentifier
                view.displayPriority = .defaultHigh
                view.markerTintColor = annotation.color
                
                view.canShowCallout = true
            }else{
                view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.glyphImage = annotation.image
                view.selectedGlyphImage = annotation.image
                view.glyphTintColor = .white
                view.clusteringIdentifier = clusterIdentifier
                view.displayPriority = .defaultHigh
                view.markerTintColor = annotation.color
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .roundedRect)
            }
            return view
        }else{
            return nil
        }
    }
    ////////////////Miscelanious Functions////////////////////////
    var welcome : WelcomeView?
    fileprivate func showWelcomeMessage(){
        //Animate from scaled down to full size with spring velociity
        DispatchQueue.main.async {
            self.welcome = WelcomeView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 100, height: 150))
            self.welcome?.center = self.view.center
            self.welcome?.delegate = self
            self.view.addSubview(self.welcome!)
            self.welcome?.layer.transform = CATransform3DMakeScale(0, 0, 0)
            
            UIView.animate(withDuration: 0.75, delay: 0.4, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                self.welcome?.layer.transform = CATransform3DMakeScale(1, 1, 1)
            })
        }
    }
    internal func handleHideWelcomeMessage(){
        //Reverse animation
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, delay: 0.1, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                self.welcome?.layer.transform = CATransform3DMakeScale(0.1, 0.1, 0.1)
                self.welcome?.alpha = 0
            }, completion: { (_) in
                self.welcome?.removeFromSuperview()
            })
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    fileprivate func styleNavBar(){
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.barTintColor = primaryBlue
        navigationController?.navigationBar.barStyle = .black
        title = "Lunch Finder"
    }
}

